import {Injectable} from '@angular/core';
import { Http } from '@angular/http';
import {BitbayService} from "../bitbay/bitbay.service";
import {Observable} from "rxjs/Rx";
import * as moment from 'moment';
import {Duration, Moment} from "moment";

// import * as Rx from 'rx';
// import {Observable} from "rxjs/Observable";
// import {Rx} from "rxjs/Rx";
// import {Rx}

export interface PoloniexTrade {
    date: number;
    high: number;
    low: number;
    open: number;
    close: number;
    volume: number;
    quoteVolume: number;
    weightedAverage: number;
}

@Injectable()
export class StockService {
    constructor(private http: Http, private dataService: BitbayService) {
    }

    public raw(): any{
        return this.dataService;
    }

    public currentExchangePlnBtc() : Observable<number> {
        return this.dataService.currentExchangePlnBtc();
    }

    //TODO move to PoloniexService
    private tradesUrl = (c1 = 'BTC', c2 = 'USDT', start: number, end: number, period: number) => `https://poloniex.com/public?command=returnChartData&currencyPair=${c1}_${c2}&start=${start}&end=${end}&period=${period}`;

    public historywWeightedAverage(c1: string, c2: string, start: Moment, end: Moment, period: Duration) : Observable<Array<number>>{
        return this.history(c1, c2, start, end, period)
            .flatMap(arr => arr)
            .map(t => t.weightedAverage)
            .toArray();
    }

    public history(c1: string, c2: string, start: Moment, end: Moment, period: Duration) : Observable<Array<PoloniexTrade>>{
        return Observable
            .of(this.tradesUrl(c1, c2, start.unix(), end.unix(), period.asSeconds()))
            .do(url => console.log(`Obtaining trade history from ${url}`))
            .flatMap(url => this.http.get(url))
            .map(response => <Array<PoloniexTrade>>response.json());
            // .do(trade => console.log(trade))
            // .subscribe();
    }
}
