import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BtcTradeHelperComponent } from './btc-trade-helper.component';

const routes: Routes = [
    { path: '', component: BtcTradeHelperComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BtcTradeHelperRoutingModule { }
