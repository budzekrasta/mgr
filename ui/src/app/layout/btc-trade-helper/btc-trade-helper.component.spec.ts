import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtcTradeHelperComponent } from './btc-trade-helper.component';

describe('BtcTradeHelperComponent', () => {
  let component: BtcTradeHelperComponent;
  let fixture: ComponentFixture<BtcTradeHelperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtcTradeHelperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtcTradeHelperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
