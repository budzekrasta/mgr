import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {BtcTradeHelperRoutingModule} from './btc-trade-helper-routing.module';
import {BtcTradeHelperComponent} from './btc-trade-helper.component';
// import { PageHeaderModule } from '../../services';

import { StockService, BitbayService } from '../../services';
import {InfoFieldComponent} from './components/info-field/info-field.component';
import { CalcSheetComponent } from './components/calc-sheet/calc-sheet.component';
import { CalcSheetRowComponent } from './components/calc-sheet/calc-sheet-row/calc-sheet-row.component';
import { InfoFieldReadOnlyComponent } from './components/info-field-read-only/info-field-read-only.component';
import { InfoFiledWithButtonComponent } from './components/info-filed-with-button/info-filed-with-button.component';
import { InfoFiledWithOptionalStreamComponent } from './components/info-filed-with-optional-streem/info-filed-with-optional-stream.component';
// import { HotTableModule } from 'ng2-handsontable';
import { CalcSyncComponent } from './components/calc-sync/calc-sync.component';
import {ChartsSheetComponent} from './components/charts-sheet/charts-sheet.component';



@NgModule({
    imports: [
        CommonModule,
        Ng2Charts,
        FormsModule,
        BtcTradeHelperRoutingModule,
        // PageHeaderModule,
        ReactiveFormsModule,
        // HotTableModule
    ],
    declarations: [BtcTradeHelperComponent, InfoFieldComponent, CalcSheetComponent, CalcSheetRowComponent, InfoFieldReadOnlyComponent, InfoFiledWithButtonComponent, InfoFiledWithOptionalStreamComponent, CalcSyncComponent, ChartsSheetComponent],
    providers: [StockService, BitbayService],
    exports: [BtcTradeHelperComponent]
})
export class BtcTradeHelperModule {
}
