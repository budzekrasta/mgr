import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoFieldReadOnlyComponent } from './info-field-read-only.component';

describe('InfoFieldReadOnlyComponent', () => {
  let component: InfoFieldReadOnlyComponent;
  let fixture: ComponentFixture<InfoFieldReadOnlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoFieldReadOnlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoFieldReadOnlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
