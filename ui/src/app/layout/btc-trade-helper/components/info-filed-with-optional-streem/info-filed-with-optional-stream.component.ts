import {ChangeDetectionStrategy, Component, forwardRef, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {InfoFiledWithButtonComponent} from "../info-filed-with-button/info-filed-with-button.component";
import {Observable} from "rxjs/Rx";
import {NG_VALUE_ACCESSOR} from "@angular/forms";

const NUMBER_CONTROL_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InfoFiledWithOptionalStreamComponent),
    multi: true
};
@Component({
    selector: 'app-info-filed-with-optional-stream',
    templateUrl: './info-filed-with-optional-stream.component.html',
    styleUrls: ['./info-filed-with-optional-stream.component.scss'],
    providers: [NUMBER_CONTROL_ACCESSOR]
})
export class InfoFiledWithOptionalStreamComponent<T> extends InfoFiledWithButtonComponent<T> implements OnChanges {

    @Input() useStream: boolean = true;
    @Input() stream$: Observable<T>;

    lastUserValue: T;
    lastStreamValue: T;

    constructor() {
        super();
    }

    onClick(event: any) {
        console.log(this.useStream)
        // this.useStream =! this.useStream;
        this.useStream = !this.useStream;
        this.locked = this.useStream;

        if (this.useStream) {
            // this.stream$.last().subscribe(x => console.log("Last stream val: " + x));
            this.lastUserValue = this.value;
            this.onChange(this.lastStreamValue);
        } else {
            this.lastStreamValue = this.value;
            this.onChange(this.lastUserValue);
        }
    }

    ngOnInit() {
        this.locked = this.useStream;
        this.subscribe();
    }

    ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {

        if (changes['stream$'] && this.stream$) {
            this.subscribe();

            //your logic work when input change
        }
    }

    subscribe() {
        if (this.stream$) {
            this.stream$.distinctUntilChanged().subscribe(value => {
                this.lastStreamValue = value;
                if (this.useStream)
                // this.value = value;
                    this.onChange(value);
            });
        }

        // TODO this is not working. why ?
        // this.stream$?.subscribe(value => {if(this.useStream) this.value = value});
    }


}
