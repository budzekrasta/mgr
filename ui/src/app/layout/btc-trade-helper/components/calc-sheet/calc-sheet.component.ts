import {Component, Input, OnInit} from '@angular/core';
import {CalcSheetRowModel} from "./calc-sheet-row/calc-sheet-row.component";
import {StockService} from "../../../../services/stock/stock.service";
import {Observable} from "rxjs/Observable";

@Component({
    selector: 'app-calc-sheet',
    templateUrl: './calc-sheet.component.html',
    styleUrls: ['./calc-sheet.component.scss']
})
export class CalcSheetComponent implements OnInit {

    @Input() positions: Array<CalcSheetRowModel>;
    @Input() currrency: string = "PLN";
    @Input() cryptoCurrrency: string = "BTC";

    currentExchangePlnBtc$: Observable<number>;


    constructor(private stockService: StockService) {
        this.currentExchangePlnBtc$ = stockService.currentExchangePlnBtc();
    }

    ngOnInit() {
    }



    getNewRowModel(): CalcSheetRowModel{
        return CalcSheetRowModel.empty();
    }

    addNewSheetRow(){
        console.log(this.stockService);
        this.positions = this.positions.concat(this.getNewRowModel());
        console.log(this.positions);
    }
}
