import {Component, Input, OnInit} from "@angular/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {StockService} from "../../../../../services/stock/stock.service";


@Component({
    selector: 'app-calc-sheet-row',
    templateUrl: './calc-sheet-row.component.html',
    styleUrls: ['./calc-sheet-row.component.scss']
})
export class CalcSheetRowComponent implements OnInit {

    @Input() prowizja: number = 0.0044;
    @Input() wspZysku: number = 1/((1-this.prowizja)**2);
    @Input() index: number;
    @Input() model: CalcSheetRowModel;
    rowGroup: FormGroup;

    constructor(private stockService: StockService, private fb: FormBuilder) {
    }

    ngOnInit() {
        this.rowGroup = this.fb.group({
            config: this.fb.group({
                ilosc: this.model.ilosc,
                bid: this.model.bid,
                cenaBid: this.model.cenaBid,
                minAsk: this.model.minAsk,
                ask: this.model.ask,
                zysk: this.model.zysk
            })
        });

        this.rowGroup.controls['config'].valueChanges.subscribe(data => {
            this.model = this.calculateModel(data);
            this.rowGroup.controls['config'].patchValue(this.model, {emitEvent: false});
        });
    }

    calculateModel(data): CalcSheetRowModel {
        var { ilosc, bid, cenaBid, minAsk, ask, zysk } = data;

        //poor transformation :P
        ilosc = this.toNumber(ilosc);
        bid = this.toNumber(bid);
        cenaBid = this.toNumber(cenaBid);
        minAsk = this.toNumber(minAsk);
        ask = this.toNumber(ask);
        zysk = this.toNumber(zysk);

        if(ilosc && bid){
            cenaBid = ilosc * bid;

            if(ask){
                zysk =  (ilosc * ask * (1 - this.prowizja) - cenaBid) - 1000;
            }
        }

        if(bid)
            minAsk = this.wspZysku * bid ;

        return new CalcSheetRowModel(ilosc, bid, cenaBid, minAsk, ask, zysk);
    }

    toNumber(f){
        var tmp;
        if(typeof(f) == 'string') {
            tmp = f.replace(',', '').replace(' ', '')
            return tmp.length === 0 ? tmp : Number(tmp);
        }

        return f;
    }
}

export class CalcSheetRowModel {
    constructor(public ilosc: number,
                public bid: number,
                public cenaBid: number,
                public minAsk: number,
                public ask: number,
                public zysk: number) {
    }

    static empty(){
        return new CalcSheetRowModel(null, null, null, null, null, null);
    }

    static all(number: number) {
        return new CalcSheetRowModel(number, number, number, number, number, number);
    }
}

