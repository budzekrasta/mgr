import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcSheetRowComponent } from './calc-sheet-row.component';

describe('CalcSheetRowComponent', () => {
  let component: CalcSheetRowComponent;
  let fixture: ComponentFixture<CalcSheetRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcSheetRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcSheetRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
