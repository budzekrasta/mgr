import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcSyncComponent } from './calc-sync.component';

describe('CalcSyncComponent', () => {
  let component: CalcSyncComponent;
  let fixture: ComponentFixture<CalcSyncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcSyncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcSyncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
