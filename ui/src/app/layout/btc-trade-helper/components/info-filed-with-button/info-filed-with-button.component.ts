import {ChangeDetectionStrategy, Component, forwardRef, Input, OnInit} from '@angular/core';
import {InfoFieldComponent} from "../info-field/info-field.component";
import {NG_VALUE_ACCESSOR} from "@angular/forms";

const NUMBER_CONTROL_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InfoFiledWithButtonComponent),
    multi: true
};

@Component({
    selector: 'app-info-filed-with-button',
    templateUrl: './info-filed-with-button.component.html',
    styleUrls: ['./info-filed-with-button.component.scss'],

    // changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [NUMBER_CONTROL_ACCESSOR]
})
export class InfoFiledWithButtonComponent<T> extends InfoFieldComponent<T> {

    @Input() btnText:string = '@';
    @Input() btnVisible:boolean = true;

    constructor() {
        super();
    }

    onClick(event: any) {

    }
}
