import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoFiledWithButtonComponent } from './info-filed-with-button.component';

describe('InfoFiledWithButtonComponent', () => {
  let component: InfoFiledWithButtonComponent;
  let fixture: ComponentFixture<InfoFiledWithButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoFiledWithButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoFiledWithButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
