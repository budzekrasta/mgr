import { Component, OnInit } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {BitbayService} from '../../../../services/bitbay/bitbay.service';
import {StockService} from '../../../../services/stock/stock.service';
import * as moment from 'moment';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';
import {Duration, Moment} from 'moment';

@Component({
  selector: 'app-charts-sheet',
  templateUrl: './charts-sheet.component.html',
  styleUrls: ['./charts-sheet.component.scss']
})
export class ChartsSheetComponent implements OnInit {

  constructor(private stockService: StockService, private bitbayService: BitbayService) { }

    /**
     * Rx question. How to have 2 observables and convert them to one, depending which one starts emitting first - take all elements from this observable
     */
  ngOnInit() {
      const c1: string = 'BTC', c2: string = 'NXT', c3: string = 'ETH',
        start: Moment = moment().subtract(7, 'days'), end: Moment = moment(), candleDuration: Duration = moment.duration(2, 'hours');

      this.stockService.historywWeightedAverage(c1, c3, start, end, candleDuration)//! poloniex; candlestick period in seconds; valid values are 300, 900, 1800, 7200, 14400, and 86400
          .subscribe(data => this.pushData({data: data, label: `${c1}-${c3}`, yAxisID: 'y-axis-1', }));

      this.stockService.history(c1, c2, start, end, candleDuration)
          .subscribe(data => {
              const price = data.map(d => d.weightedAverage);
              const dateLabels = data.map(d => moment.unix(d.date).format('MM/DD/YYYY hh:mm:ss'));

              this.lineChartLabels = dateLabels;

              this.pushData({data: price, label: `${c1}-${c2}`, yAxisID: 'y-axis-0', });

              console.log(this.lineChartLabels);
              console.log(this.lineChartData);

          });





            // console.log(this)
            // console.log(this.lineChartData)
            // console.log(data)
            // console.log([{data: data, label: `${c1}-${c2}`}]);
            //   // Observable.from(data).min().subscribe(min => {
            //       console.log(_.min(data));
            //       this.lineChartOptions.scales.yAxes[0].ticks.min = _.min(data);
            //   // });
            //   // Observable.from(data).max().subscribe(max => {
            //       console.log(_.max(data));
            //       this.lineChartOptions.scales.yAxes[0].ticks.max = _.max(data);
            //   // });
            //   this.lineChartLabels = _.range(0, data.length).map(i => `${i}`);
            //
            //   this.lineChartData = [{data: data, label: `${c1}-${c2}`}];
            //
            //
            //     console.log(this.lineChartData);
            //   console.log(this.lineChartData[0].data.length);
          // } );



  }

  private pushData(data: any){
      if (!this.lineChartData)
          this.lineChartData = [];
      this.lineChartData.push(data);
  }
    // lineChart
    public lineChartData: Array<any>;
    public lineChartLabels: Array<any>;
    //     = [
    //     {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    //     {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
    //     {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
    // ];
    // public lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
    public lineChartOptions: any = {
        responsive: true,
        scales: {
            yAxes: [{
                position: 'left',
                'id': 'y-axis-0'//,
                // ticks: {
                //     min: 0.00001000,
                //     max: 0.00003000
                // }
            }, {
                    position: 'right',
                    'id': 'y-axis-1'//,
                    // ticks: {
                    //     min: 0.00001000,
                    //     max: 0.00003000
                    // }
            }]
        }
    };

// }    options: {
//
//     }
    public lineChartColors: Array<any> = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];
    public lineChartLegend: boolean = true;
    public lineChartType: string = 'line';

    // events
    public chartClicked(e: any): void {
        // this.stockServie.currentExchangePlnBtc().subscribe((exc) => console.log("Current exchange: " + exc))
        // console.log(this.seriesAData());
        // // console.log(e);
    }

    public chartHovered(e: any): void {
        // console.log(e);
    }


}
