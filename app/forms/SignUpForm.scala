package forms

import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.{ Constraint, Invalid, Valid, ValidationError }

/**
 * The form which handles the sign up process.
 */
object SignUpForm {

  val allNumbers = """\d*""".r
  val noSpecialChar = """[A-Za-z0-9]*""".r
  val anyUppercaseChar = """[^A-Z]*""".r
  val fiveCharsAtLeast = """.{0,4}""".r
  //  val noSpecialChar = """[^!#$%&'()*+,-./:;<=>?@[\]^_`{|}~]""".r

  val passwordCheckConstraint: Constraint[String] = Constraint("constraints.passwordcheck")({
    plainText =>
      val errors = plainText match {
        case allNumbers() => Seq(ValidationError("Password is all numbers"))
        case noSpecialChar() => Seq(ValidationError("Password has no special characters"))
        case anyUppercaseChar() => Seq(ValidationError("Password has no uppercase letter"))
        case fiveCharsAtLeast() => Seq(ValidationError("Password has less than 5 characters"))
        case _ => Nil
      }
      if (errors.isEmpty) {
        Valid
      } else {
        Invalid(errors)
      }
  })

  /**
   * A play framework form.
   */
  val form = Form(
    mapping(
      "firstName" -> nonEmptyText,
      "lastName" -> nonEmptyText,
      "email" -> email,
      "password" -> (nonEmptyText).verifying(passwordCheckConstraint)
    )(Data.apply)(Data.unapply)
  )

  /**
   * The form data.
   *
   * @param firstName The first name of a user.
   * @param lastName The last name of a user.
   * @param email The email of the user.
   * @param password The password of the user.
   */
  case class Data(
    firstName: String,
    lastName: String,
    email: String,
    password: String)
}
