package utils

import javax.inject.Inject

import play.api.http.HttpFilters
import play.api.mvc.EssentialFilter
import play.filters.csrf.CSRFFilter
import play.filters.headers.SecurityHeadersFilter

/**
 * Provides filters.
 */
//Temporally disabled CSP - angular files must be added to headers config
//class Filters @Inject() (csrfFilter: CSRFFilter, securityHeadersFilter: SecurityHeadersFilter) extends HttpFilters {
//  override def filters: Seq[EssentialFilter] = Seq(csrfFilter, securityHeadersFilter)
//}
class Filters @Inject() (csrfFilter: CSRFFilter) extends HttpFilters {
  override def filters: Seq[EssentialFilter] = Seq(csrfFilter)
}
