import java.util.UUID

import scala.concurrent.Future
import javax.inject._

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.util.PasswordHasherRegistry
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import models.User
import models.services.{ AuthTokenService, UserService }
import play.api.inject.ApplicationLifecycle
import scala.concurrent.ExecutionContext.Implicits.global

import scala.util.{ Failure, Success }

// This creates an `ApplicationStart` object once at start-up and registers hook for shut-down.
@Singleton
class ApplicationStart @Inject() (
  lifecycle: ApplicationLifecycle,
  userService: UserService,
  passwordHasherRegistry: PasswordHasherRegistry,
  authInfoRepository: AuthInfoRepository,
  authTokenService: AuthTokenService) {

  //  bootstrapDb()

  // Shut-down hook
  lifecycle.addStopHook { () =>
    Future.successful(())
  }

  def bootstrapDb() = {
    val loginInfo = LoginInfo(CredentialsProvider.ID, "budzek@tlen.pl")
    val authInfo = passwordHasherRegistry.current.hash("haslo")

    val userFuture = userService.save(User(
      userID = UUID.randomUUID(),
      loginInfo = loginInfo,
      firstName = Some("Artur"),
      lastName = Some("Budzyński"),
      fullName = Some("Artur Budzyński"),
      email = Some("budzek@tlen.pl"),
      avatarURL = None,
      activated = true
    ))

    authInfoRepository.add(loginInfo, authInfo)
    userFuture.onComplete {
      case Success(user) => authTokenService.create(user.userID)
      case Failure(t) => println("Error during user bootstraping: " + t.getMessage)
    }

  }
}